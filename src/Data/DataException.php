<?php

namespace Dero\Data;

/**
 * Internal exception around data interaction problems
 */
class DataException extends \Exception
{
}
